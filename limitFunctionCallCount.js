function limitFunctionCallCount(cb, n) {
    try {

        let count = 0;
        if (cb === undefined || n === undefined) {
            throw 'arguments defined incorrectly';
        } else {
            return function inner(value1, value2) {
                count++
                if (count <= n) {
                    return cb(value1, value2)

                } else {
                    return null
                }
            }
        }

    } catch (error) {
        console.log(error)
    }
}


module.exports = limitFunctionCallCount


