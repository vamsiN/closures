const limitFunctionCallCount = require('../limitFunctionCallCount')

function add(a, b) {
    return a + b;
}

limitedFunction1 = limitFunctionCallCount()



try {

    console.log(limitedFunction1(1, 2))
    console.log(limitedFunction1(10,20))
    console.log(limitedFunction1(100, 200))
    console.log(limitedFunction1(1000, 2000))


} catch (error) {
    console.log(error.message)
}