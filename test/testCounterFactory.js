const counterFactory = require('../counterFactory')


let first_counter = counterFactory(100)
console.log(first_counter.increment(6))
console.log(first_counter.decrement(5))


let wickets_counter = counterFactory(10)
console.log(wickets_counter.increment(3))
console.log(wickets_counter.decrement(1))

let runs_counter = counterFactory()
console.log(runs_counter.increment(6))

console.log(first_counter.increment())
console.log(first_counter.decrement())