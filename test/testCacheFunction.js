const cacheFunction = require('../cacheFunction')

function square (a){
    return a**2
}

function add (a,b){
    console.log("im in cb")
    return a+b
}

// square_function = cacheFunction(square)
// console.log(square_function(2))
// console.log(square_function(5))
// console.log(square_function(10))
// console.log(square_function(2))

add_function = cacheFunction(add)
console.log(add_function(1,2))
console.log(add_function(10,20))
console.log(add_function(100,200))
console.log(add_function(1,2))